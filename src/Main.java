import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    private static final int mRaindropInLine = 5;

    private static final int mCanvasWidth = 800;
    private static final int mCanvasHeight = 500;

    private static final int mBackgroundColor = 0xF2E5F2;

    private static final int[][] mRaindropProximity = {
    //       Width    Height    Color Index  Speed
            {2,       10,        0,          2    },
            {2,       12,        0,          3    },
            {2,       14,        1,          4    },
            {3,       16,        2,          4    },
            {3,       18,        3,          5    },
            {3,       20,        4,          6    }
    };

    private static final Color[] mColors = {
            new Color(0xB266B2),
            new Color(0xA05BA0),
            new Color(0x8E518E),
            new Color(0x7C477C),
            new Color(0x6A3D6A)
    };

    private int mCreateNewRaindropLineAt = 5;
    private int mCreateNewRaindropLineCounter = 0;

    private JFrame mJFrame;
    private Canvas mCanvas;
    private Graphics mCanvasGraphics;

    private Random mRandomGenerator;

    private ArrayList<Raindrop> mRaindropsList;

    public Main(){
        mJFrame = new JFrame("Purple Rain");
        mCanvas = new Canvas();

        mCanvas.setSize(mCanvasWidth, mCanvasHeight);
        mCanvas.setBackground(new Color(mBackgroundColor));

        mRaindropsList = new ArrayList<>();

        mRandomGenerator = new Random();
    }

    public void start(){
        mJFrame.setSize(mCanvasWidth, mCanvasHeight);
        mJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mJFrame.add(mCanvas);

        mJFrame.setVisible(true);

        run();
    }

    private void run(){
        new Thread(() -> {
            mCanvasGraphics = mCanvas.getGraphics();

            while(true){
                updateView();
            }
        }).start();
    }

    private void updateView(){
        updateCanvas();


        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void updateCanvas(){
        if(mCreateNewRaindropLineCounter == mCreateNewRaindropLineAt) {
            addNextRaindropLine();
            mCreateNewRaindropLineCounter = 0;
            System.gc();

        } else {
            mCreateNewRaindropLineCounter++;
        }

        Raindrop raindrop;
        for (int i = 0; i < mRaindropsList.size(); i++) {
            raindrop = mRaindropsList.get(i);

            mCanvasGraphics.setColor(mColors[mRaindropProximity[raindrop.mProximity][2]]);

            mCanvasGraphics.clearRect(
                    raindrop.mX,
                    raindrop.mY,
                    mRaindropProximity[raindrop.mProximity][0],
                    mRaindropProximity[raindrop.mProximity][1]
            );

            raindrop.mY += mRaindropProximity[raindrop.mProximity][3];
            if(raindrop.mY > mCanvasHeight){
                mRaindropsList.remove(i);

            } else {
                mCanvasGraphics.fillRect(
                        raindrop.mX,
                        raindrop.mY,
                        mRaindropProximity[raindrop.mProximity][0],
                        mRaindropProximity[raindrop.mProximity][1]
                );
            }
        }
    }

    private void addNextRaindropLine(){
        for (int i = 0; i < mRaindropProximity.length; i++) {
            for (int j = 0; j < mRaindropInLine; j++) {
                Raindrop raindrop = new Raindrop();
                raindrop.setX(mRandomGenerator.nextInt(mCanvasWidth));
                raindrop.setY(-mRaindropProximity[i][1] - mRaindropProximity[i][3]);
                raindrop.setProximity(i);

                mRaindropsList.add(raindrop);
            }
        }
    }

    public static void main(String[] args){
        new Main().start();
    }

    private class Raindrop {
        public int mX;
        public int mY;
        public int mProximity;

        public void setX(int X) {
            this.mX = X;
        }

        public void setY(int Y) {
            this.mY = Y;
        }

        public void setProximity(int Proximity) {
            this.mProximity = Proximity;
        }
    }
}